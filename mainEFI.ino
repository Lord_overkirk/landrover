
#include <Arduino.h>

#include <math.h>


unsigned long start;
unsigned long elapsed;
unsigned long RPM = 0;
float RTT;
int numOfCyl = 8 ; 
int pin = 4; 
bool RIGHT = true;
int BANK_COUNTER = 0;

void setup()
{
  Serial.begin(9600);
  pinMode(pin, INPUT);
  pinMode(5, OUTPUT); //Left bank injectoren (2,4,6,8)
  pinMode(6, OUTPUT); //Right bank injectoren (1,3,5,7)
//  pinMode(3, OUTPUT); //Pin voor relais brandstofpomp
//  digitalWrite(3, LOW);


  digitalWrite(5, LOW);
  digitalWrite(6, LOW);
 
  start = micros();
  
//  digitalWrite(3, LOW); //Brandstofpomp aan
//  delay(3000);  //Brandstofpomp aan gedurende 3 seconden
//  digitalWrite(3, HIGH); //Brandstofpomp uit
}

int speedCalc() {
  elapsed = micros() - start;
  start = micros();

  //RPM = ((120 * (1000000 / elapsed)) / numOfCyl); //MS to RPM +-30RPM Res
  int rpm = ((12*(10000000/elapsed)) /numOfCyl); //uS  +- 3RPM resolution
  return rpm;
}

void fireBank(int pin) {
    int microSecondsDelay = 2200; //Tijd in ms injector open

    digitalWrite(pin, HIGH);
    delayMicroseconds(microSecondsDelay);
    digitalWrite(pin, LOW);
}

void injectionTiming() {
    if (BANK_COUNTER == 0) {
        fireBank(5);
   } else if (BANK_COUNTER == 2) {
        fireBank(6);
    } else if (BANK_COUNTER == 4) {
        fireBank(5);
    } else if (BANK_COUNTER == 6) {
        fireBank(6);
    } else if (BANK_COUNTER == 7) {
        BANK_COUNTER = 0;
        return;
    }
    BANK_COUNTER++;
}

void realProcess() {
    if (digitalRead(pin) == HIGH) {
        injectionTiming();
    }

    if ((micros() - start) >= 500000) {
        RPM=0;
    }
}

void simulationRoutine() {
    for (int i = 0; i < 100000; i++) {
        if (digitalRead(pin) == HIGH || i == 0) {
            injectionTiming();
        }

        if ((micros() - start) >= 500000) {
            RPM=0;
        }
    }
}

//void brandstofpomp() {
//  if (digitalRead(pin) == HIGH) { //Als signaal van coil aanwezig is, 
//    digitalWrite(3, LOW); //Dan de brandstofpomp aan
//  } 
//}

void loop() {
   realProcess();
//   brandstofpomp();
//    simulationRoutine();
}
